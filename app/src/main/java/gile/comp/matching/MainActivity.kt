package gile.comp.matching

import android.os.Bundle
import android.widget.ImageButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import gile.comp.matching.Model.Card
import gile.comp.matching.R.drawable.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var imageButtons: List<ImageButton>
    private lateinit var imageCards: List<Card>
    private var indexOneInvertedMapPointer: Int? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val images: MutableList<Int> = mutableListOf(cat, lion, hare, monkey, fox, dog)
        images.addAll(images)
        images.shuffle()
        imageButtons = listOf(
            card_1,
            card_2,
            card_3,
            card_4,
            card_5,
            card_6,
            card_7,
            card_8,
            card_9,
            card_10,
            card_11,
            card_12
        )

        imageCards = imageButtons.indices.map { index ->
            Card(images[index])
        }

        imageButtons.forEachIndexed { index, buttons ->
            buttons.setOnClickListener {
                updateData(index)
                updateSight()
            }
        }
    }

    private fun checkForAccordance(position1: Int, position2: Int) {
        if (imageCards[position1].symbol == imageCards[position2].symbol) {
            imageCards[position1].isAccordance = true
            imageCards[position2].isAccordance = true
            Toast.makeText(this, "You found a pair, great!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun reestablishCards() {
        for (card in imageCards) {
            if (!card.isAccordance) {
                card.isInverted = false
            }
        }
    }

    private fun updateData(cardPosition: Int) {
        val card = imageCards[cardPosition]
        if (card.isInverted) {
            Toast.makeText(this, "Wrong move!", Toast.LENGTH_SHORT).show()
            return
        }
        indexOneInvertedMapPointer = if (indexOneInvertedMapPointer == null) {
            reestablishCards()
            cardPosition
        } else {
            checkForAccordance(indexOneInvertedMapPointer!!, cardPosition)
            null
        }
        card.isInverted = !card.isInverted
    }

    private fun updateSight() {
        imageCards.forEachIndexed { index, card ->
            val button = imageButtons[index]
            if (card.isAccordance) {
                button.alpha = 0.5f
            }
            button.setImageResource(if (card.isInverted) card.symbol else default_icon)
        }
    }
}