package gile.comp.matching.Model

data class Card(var symbol: Int, var isInverted: Boolean = false, var isAccordance: Boolean = false)
