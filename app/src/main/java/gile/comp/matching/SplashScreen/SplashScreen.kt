package gile.comp.matching.SplashScreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import gile.comp.matching.R
import gile.comp.matching.StartGameActivity

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        val SPLASH_SCREEN_TIME: Long = 3000
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        Handler().postDelayed({
            startActivity(Intent(this, StartGameActivity::class.java))
            finish()
        }, SPLASH_SCREEN_TIME)
    }
}